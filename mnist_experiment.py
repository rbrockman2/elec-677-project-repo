import os
import matplotlib.cm as cm

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.utils.visualize_util import plot

from keras import callbacks

from visualization_tools import *
from sklearn.metrics import classification_report
from keras import backend as k

import matplotlib.pyplot as plt

class MNIST_Experiment(object):
    """Trains a simple convnet on the MNIST dataset.

    Gets to 99.25% test accuracy after 12 epochs
    (there is still a lot of margin for parameter tuning).
    16 seconds per epoch on a GRID K520 GPU.

    Based on Keras example mnist_cnn.py.
    Visualization based on heavy modification from here:

    https://github.com/julienr/ipynb_playground/blob/master/keras/convmnist/keras_cnn_mnist_v1.ipynb
    """
    def __init__(self):
        self.nb_classes = 10  # 10 digits
        self.sample_image_number = 4600  # number of arbitrary image in training set
        self.sample_image = None  # arbitrary image in training set
        self.x_train = None  # normalized and flattened image data (training set)
        self.x_test = None  # normalized and flattened image data (test set)
        self.y_train = None  # image labels in one-hot vector form (training set)
        self.y_test = None  # image labels in one-hot vector form (test set)
        self.raw_y_test = None  # image labels in decimal form

        self.model = None  # keras model of the convolutional neural network
        self.batch_size = 128
        self.nb_epoch = 12

        self.score = None  # computed score performance on test set

        # hooks for visualizing intermediate layers
        self.conv_layer_indices = []
        self.relu_layer_indices = []

        self.debug = False  # debug mode

        np.random.seed(1337)  # for reproducibility
        np.set_printoptions(precision=5, suppress=True)

    def load_data(self):
        """Loads the data set of images and labels."""
        # Data is split between training and test sets.
        (raw_x_train, raw_y_train), (raw_x_test, raw_y_test) = mnist.load_data()
        self.raw_y_test = raw_y_test

        flat_x_train = raw_x_train.reshape(raw_x_train.shape[0], 1, 28, 28)
        flat_x_test = raw_x_test.reshape(raw_x_test.shape[0], 1, 28, 28)
        x_train = flat_x_train.astype("float32")
        x_test = flat_x_test.astype("float32")

        # normalize
        self.x_train = x_train / 255
        self.x_test = x_test / 255

        print('X_train shape:', self.x_train.shape)
        print(self.x_train.shape[0], 'train samples')
        print(self.x_test.shape[0], 'test samples')

        # Convert labels to one-hot.
        self.y_train = np_utils.to_categorical(raw_y_train, self.nb_classes)
        self.y_test = np_utils.to_categorical(raw_y_test, self.nb_classes)

        if self.debug:
            pl.imshow(self.x_train[self.sample_image_number, 0], interpolation='nearest', cmap=cm.binary)
            print("label : ", self.y_train[self.sample_image_number, :])
            print("raw label : ", raw_y_train[self.sample_image_number])

        # Select sample image.
        self.sample_image = self.x_test[self.sample_image_number:self.sample_image_number + 1]

    def build_model(self):
        """Constructs the cnn model layer by layer."""
        model = Sequential()

        # First layer needs the shape of its input specified, later layers just use the output dimensions
        # of the previous layer.
        # Layer 0
        model.add(Convolution2D(32, 3, 3, border_mode='valid', input_shape=self.x_train.shape[1:]))
        self.conv_layer_indices.append(len(model.layers) - 1)

        # The Dropout is not in the original keras example, it's just here to demonstrate how to
        # correctly handle train/predict phase difference when visualizing convolutions below
        # Layer 1
        model.add(Dropout(0.1))

        model.add(Activation('relu'))
        self.relu_layer_indices.append(len(model.layers) - 1)

        q = Convolution2D(32, 3, 3)
        model.add(q)

        weights = q.W.get_value(borrow=True)
        w = np.squeeze(weights)
        print("weight set shape : ", w.shape)

        #model.add(Convolution2D(32, 3, 3))
        self.conv_layer_indices.append(len(model.layers) - 1)

        model.add(Activation('relu'))
        self.relu_layer_indices.append(len(model.layers) - 1)

        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())
        model.add(Dense(128))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        model.add(Dense(self.nb_classes))
        model.add(Activation('softmax'))

        model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
        plot(model, to_file='model.png')  # Create diagram of model.

        self.model = model

    def train_model(self):
        """Train the constructed model."""
        # allows real-time display of training progress
        remote = callbacks.RemoteMonitor(root='http://localhost:9000')

        weights_filename = 'mnist_cnn_weights_v1.hdf'  # model can be saved to disk
        if False and os.path.exists(weights_filename):
            # Just change the True to false to force re-training
            print('Loading existing weights')
            self.model.load_weights(weights_filename)
        else:
            self.model.fit(self.x_train, self.y_train, batch_size=self.batch_size, nb_epoch=self.nb_epoch,
                      verbose=1, validation_data=(self.x_test, self.y_test), callbacks=[remote])
            self.model.save_weights(weights_filename, overwrite=True)

    def test_model(self):
        """Evaluate how well the model performs on the test set."""
        score = self.model.evaluate(self.x_test, self.y_test, verbose=0)

        print('Test score:', score[0])
        print('Test accuracy:', score[1])
        self.score = score

        if self.debug:
            print(self.model.predict(self.x_test[1:5]))
            print(self.y_test[1:5])

        y_pred = self.model.predict(self.x_test)
        # Convert one-hot to index
        index_y_pred = np.argmax(y_pred, axis=1)

        if self.debug:
            print("args")
            print(self.y_test[1:5])
            print(index_y_pred[1:5])
            print(y_pred[1:5])

        print(classification_report(self.raw_y_test, index_y_pred))

    def display_conv_layer_weights(self, conv_layer_number):
        """conv_layer_number:  which convolution layer to display the weights for"""
        layer_index = self.conv_layer_indices[conv_layer_number]

        # Visualize weights
        weights = self.model.layers[layer_index].W.get_value(borrow=True)
        w = np.squeeze(weights)
        print("weight set " + str(conv_layer_number) + " shape : ", w.shape)

        pl.figure(figsize=(15, 15))
        pl.title('Weight set ' + str(conv_layer_number))
        nice_imshow(pl.gca(), make_mosaic(w, 6, 6), cmap=cm.binary)

# Gradient histograms

    def display_relu_layer_result(self, relu_layer_number, image):
        layer_index = self.relu_layer_indices[relu_layer_number]

        # k.learning_phase() is a flag that indicates if the network is in training or
        # predict phase. It allows layer (e.g. Dropout) to only be applied during training
        inputs = [k.learning_phase()] + self.model.inputs
        _layer_function = k.function(inputs, [self.model.layers[layer_index].output])

        def layer_function(input_image):
            # The [0] is to disable the training phase flag
            return _layer_function([0] + [input_image])

        # Visualize convolution result (after activation) for sample image
        conv_result = np.squeeze(layer_function(image))
        print("convolution result " + str(relu_layer_number) + " shape : ", conv_result.shape)

        pl.figure(figsize=(15, 15))
        pl.title('Convolution Result ' + str(relu_layer_number))
        nice_imshow(pl.gca(), make_mosaic(conv_result, 6, 6), cmap=cm.binary)

    def visualize(self):
        """Displays information about the intermediate layers in graphical form.
        Uses visualization_tools.py."""
        pl.figure()
        pl.title('input')
        nice_imshow(pl.gca(), np.squeeze(self.sample_image), vmin=0, vmax=1, cmap=cm.binary)

        for i in range(len(self.relu_layer_indices)):
            self.display_relu_layer_result(i, self.sample_image)


        layer_index = self.conv_layer_indices[1]

        # Visualize weights
        weights = self.model.layers[layer_index].W.get_value(borrow=True)
        w = np.squeeze(weights)
        print("weight set 1 shape : ", w.shape)

        print(w[1, 1, :, :])
        print(w[1, 2, :, :])
        print(w[2, 1, :, :])

        #for i in range(len(self.conv_layer_indices)):
        #    self.display_conv_layer_weights(i)
        plt.show(block=True)  # displays graphs

myExperiment = MNIST_Experiment()
myExperiment.load_data()
myExperiment.build_model()
myExperiment.train_model()
myExperiment.test_model()
myExperiment.visualize()
